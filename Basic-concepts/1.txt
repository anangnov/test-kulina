#### A. 
// Should be pass in parameter as new variable
class Test
{
   protected $a;
   public function __constructor(Word $a)
   {
      $this->a = $a;
   }
}

// 2
// pass params value default
function a(x, z) {
  return x + z;
}

function b(j) {
  if (j == 0) {
    j = 1;
  }

  a(j, 2);
}

// this is should be
function a(x = 1, z) {
  return x + z;
}

function b() {
  a(2);
}

### B
Dependency Injection is a design pattern. which one is there client class, service class, and injector class.
Client class is the client class (dependent class) is a class which depends on the service class. 
Service class is the service class (dependency) is a class that provides service to the client class.
Injector class is the injector class injects the service class object into the client class.

