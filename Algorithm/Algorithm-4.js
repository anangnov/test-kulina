// This is number 4

function lampsSwitches(n) {
  let result = [];

  for (let i = 0; i < n; i++) {
    let mod = n % (i + 1);
    let x = n - mod;

    result[i] = x / (i + 1);
  }
  
  let sum = result.reduce((a, b) => a + b, 0);
  return sum;
}

console.log(lampsSwitches(100))