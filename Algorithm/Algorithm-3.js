// This is number 3

// Pseudocode

// 'input' number
// set empty variable 'a' string
// for line 1 to input
// for line line 1 to input
// a += j
// end
// a += '\n'
// end
// return a

// Source Code
function patternJs(input) { 
  let a = '';
  for (let i = 1; i <= input; i++) {
    for (let j = i; j <= input; j++) {
      a += j;
    }

    a += '\n';
  }

  return a;
}

console.log(patternJs(4))